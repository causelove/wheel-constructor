import { FunctionComponent, ReactElement } from "react";

import styles from './index.module.scss';

interface Properties {
  aside: ReactElement;
}

export const DefaultLayout: FunctionComponent<Properties> = ({ children, aside }) => {
  return (
    <div className={styles['default-layout']}>
      {aside}
      <div className={styles['default-layout__children']}>
        {children}
      </div>
    </div>
  );
};
