import { FunctionComponent } from 'react';

import styles from './index.module.scss';

export const HomeLayout: FunctionComponent<unknown> = ({ children }) => {
  return (
    <div className={styles['home-layout']}>
      {children}
    </div>
  );
};
