import { FunctionComponent } from "react";

import styles from './index.module.scss';

interface Properties {
  title: string;
}

export const DefaultBackground: FunctionComponent<Properties> = ({ children, title }) => {
  return (
    <div className={styles['default-background']}>
      <span className={styles['default-background__title']}>{title}</span>
      {children}
    </div>
  );
};
