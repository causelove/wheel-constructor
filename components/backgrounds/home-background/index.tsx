import { FunctionComponent } from 'react';

import styles from './index.module.scss';

export const HomeBackground: FunctionComponent<unknown> = ({ children }) => {
  return (
    <div className={styles['home-background']}>
      {children}
    </div>
  );
};
