import { FunctionComponent } from 'react';

import styles from './index.module.scss';

interface Properties {
  className?: string;
  onClick?: () => unknown;
}

export const Button: FunctionComponent<Properties> = ({ onClick, children, className }) => {
  return (
    <button className={`${styles['button']} ${className}`} onClick={onClick}>
      {children}
    </button>
  );
};
