import { FunctionComponent } from "react";

import styles from './index.module.scss';

interface Properties {
  className?: string;
}

export const Card: FunctionComponent<Properties> = ({ children, className }) => {
  return (
    <div className={`${styles.card} ${className}`}>
      {children}
    </div>
  );
};
