import { DefaultBackground } from "../../components/backgrounds/default-background";
import { DefaultLayout } from "../../components/layouts/default-layout";
import { Aside } from "../../components/aside";
import { Card } from "../../components/card";

import styles from './index.module.scss';
import { Button } from "../../components/button";

export default function Projects() {
  return (
    <DefaultBackground title="PROJECTS">
      <DefaultLayout aside={<Aside/>}>
        <Card className={`${styles['projects__card']} shadow`}>
          <span className={styles['projects__card-title']}>Untitled</span>
          <Button>
            
          </Button>
        </Card>
      </DefaultLayout>
    </DefaultBackground>
  );
};
